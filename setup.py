from configparser import ConfigParser
from setuptools import setup


cfg = ConfigParser()
cfg.read('acsoo.cfg')


setup(
    version=cfg.get('acsoo', 'series') + '.' + cfg.get('acsoo', 'version'),
    name='odoo-addons-laa',
    description='Laa Odoo Addons',
    setup_requires=['setuptools-odoo'],
    install_requires=[
        'click-odoo-contrib>=1.4.1',
    ],
    odoo_addons=True,
)
