# Copyright (C) 2014-2015  Grupo ESOC <www.grupoesoc.es>
# Copyright 2017-Apertoso N.V. (<http://www.apertoso.be>)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
from odoo import api, fields, models


class ResPartner(models.Model):
    """Partner with birth date in date format."""
    _inherit = "res.partner"

    birthdate_date = fields.Date("Birthdate")
    federation_number = fields.Char()
    member_number = fields.Char(compute="_compute_member_number")

    @api.depends() # depends on id
    def _compute_member_number(self):
        for p in self:
            p.member_number = "AA%03d" % p.id
