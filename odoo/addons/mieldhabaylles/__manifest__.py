# Copyright 2019 Stéphane
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'Mieldhabaylles',
    'description': """
        Gestion des ventes""",
    'version': '13.0.1.0.0',
    'license': 'AGPL-3',
    'author': 'Stéphane',
    'depends': [
        "product",
    ],
    'data': [
        'security/ventes.xml',
        'views/ventes.xml',
    ],
    'demo': [
    ],
}
