# Copyright 2019 Stéphane
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, fields, models, _


class Ventes(models.Model):

    _name = 'ventes'
    _description = 'Ventes'  # TODO

    date = fields.Date()
    partner_id = fields.Many2one(comodel_name="res.partner", string="Client", required=True)
    commande = fields.Integer(required=True, string="Commandé")
    livre = fields.Integer(string="Livré")
    product_id =fields.Many2one(comodel_name="product.template", string="Produit", required=True)
    price = fields.Float(related="product_id.list_price")
    total_price = fields.Float(compute="_compute_total_price")
    reste = fields.Integer(compute="_compute_reste", store=True)
    prix_paye = fields.Float()
    paye = fields.Boolean(compute="_compute_paye", store=True)

    @api.multi
    @api.depends("commande","product_id","product_id.list_price")
    def _compute_total_price(self):
        for vente in self:
            vente.total_price=vente.commande*vente.product_id.list_price

    @api.multi
    @api.depends("commande","livre")
    def _compute_reste(self):
        for vente in self:
            reste=vente.commande-vente.livre
            if reste<0:
                reste=0
            vente.reste=reste
    
    @api.multi
    @api.depends("prix_paye", "commande", "product_id", "product_id.list_price")
    def _compute_paye(self):
        for vente in self:
            vente.paye = vente.prix_paye == vente.total_price
