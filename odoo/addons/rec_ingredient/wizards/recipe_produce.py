# Copyright 2019 Denis
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, fields, models, _


class RecipeProduce(models.TransientModel):

    _name = 'recipe.produce'
    _description = 'Truc'

    name = fields.Char(related="product_id.name")
    unit_id = fields.Many2one(comodel_name="ingredient.unit")
    category_id = fields.Many2one(related="product_id.category_id")
    product_id = fields.Many2one(comodel_name="product.template", readonly=True)
    quantity = fields.Float(default=1)
    line_ids = fields.One2many(comodel_name="recipe.produce.line", inverse_name="wizard_id", readonly=True)

    @api.onchange("product_id")
    def _onchange_product_id(self):
        for wiz in self:
            wiz.unit_id = wiz.product_id.unit_id

    @api.multi
    @api.onchange("quantity", "unit_id")
    def onchange_quantity(self):
        for line in self.line_ids:
            if self.category_id:
                line.cost = line.ingredient_id.cost * line.wizard_id.quantity / self.product_id.unit_id.coefficient * self.unit_id.coefficient
                line.quantity = line.ingredient_id.quantity * line.wizard_id.quantity / self.product_id.unit_id.coefficient * self.unit_id.coefficient
            else:
                line.cost = line.ingredient_id.cost * line.wizard_id.quantity
                line.quantity = line.ingredient_id.quantity * line.wizard_id.quantity
            line.name = "{}, {}{} ({}{})".format(
                line.product_id.display_name, line.quantity, line.type.name, line.cost, line.currency_id.symbol)

    @api.model
    def _create_from_product(self, product):
        lines = []
        for line in product.ingredient_ids:
            lines.append((0, 0, {
                "ingredient_id": line.id, 
                "cost": line.cost,
                "quantity": line.quantity,
                "name": line.name,
            }))
        wiz = self.create({
            "product_id": product.id,
            "line_ids": lines,
            "unit_id": product.unit_id.id,
        })
        action = wiz.get_formview_action()
        action["target"] = "new"
        action["name"] = "Production"
        return action
    
    @api.multi
    def print(self):
        self.ensure_one()
        self.onchange_quantity() # ing as readonly, onchange not updated
        return self.env.ref("rec_ingredient.report_recipe_wizard_pdf").read()[0]


class RecipeProduceLine(models.TransientModel):

    _name = 'recipe.produce.line'
    _description = 'Truc'

    wizard_id = fields.Many2one(comodel_name="recipe.produce", required=True, ondelete="cascade")
    ingredient_id = fields.Many2one(comodel_name="product.ingredient", readonly=True)

    product_id = fields.Many2one(related="ingredient_id.product_id")
    type = fields.Many2one(related="ingredient_id.type")
    currency_id = fields.Many2one(related="ingredient_id.currency_id")
    cost = fields.Monetary()
    quantity = fields.Float()
    name = fields.Char()
