# Copyright 2019 Denis
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, fields, models, _


class IngredientUnit(models.Model):

    _name = 'ingredient.unit'
    _description = 'Ingredient Unit'

    name = fields.Char(required=True)
    coefficient = fields.Float(required=True)
    category_id = fields.Many2one(comodel_name="ingredient.unit.category", required=True)
