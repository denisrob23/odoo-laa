# Copyright 2019 Denis
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, fields, models, _


class ProductIngredient(models.Model):

    _name = 'product.ingredient'
    _description = 'Product Ingredient'

    name = fields.Char(compute="_compute_name")
    recipe_id = fields.Many2one(comodel_name="product.template", required=True, ondelete="cascade")
    product_id = fields.Many2one(comodel_name="product.template", required=True)
    quantity = fields.Float()
    type = fields.Many2one(comodel_name="ingredient.unit")
    category_id = fields.Many2one(related="product_id.category_id")
    cost = fields.Monetary(compute="_compute_cost", store=True)
    currency_id = fields.Many2one(related="product_id.currency_id")

    @api.onchange("product_id")
    def _onchange_product_id(self):
        for ing in self:
            ing.type = ing.product_id.unit_id

    @api.multi
    @api.depends("product_id", "product_id.standard_price", "product_id.unit_id", "product_id.unit_id.coefficient", "quantity", "type", "type.coefficient")
    def _compute_cost(self):
        for ing in self:
            if ing.category_id:
                ing.cost = ing.product_id.standard_price * ing.quantity / ing.product_id.unit_id.coefficient * ing.type.coefficient
            else:
                ing.cost = ing.product_id.standard_price * ing.quantity

    @api.multi
    @api.depends("product_id", )
    def _compute_name(self):
        for ing in self:
            ing.name = "{}, {}{} ({}{})".format(ing.product_id.display_name, ing.quantity, ing.type.name, ing.cost, ing.currency_id.symbol)
