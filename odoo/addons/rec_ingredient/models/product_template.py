# Copyright 2019 Denis
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, fields, models, _


class ProductTemplate(models.Model):

    _inherit = 'product.template'

    ingredient_ids = fields.One2many(
        comodel_name="product.ingredient", inverse_name="recipe_id")
    standard_price = fields.Float(compute="_compute_standard_price", inverse="_inverse_standard_price", readonly=False)
    stored_standard_price = fields.Float()
    unit_id = fields.Many2one(comodel_name="ingredient.unit")
    category_id = fields.Many2one(related="unit_id.category_id")
    processus = fields.Text()

    @api.multi
    @api.depends("stored_standard_price", "ingredient_ids", "ingredient_ids.cost")
    def _compute_standard_price(self):
        for product in self:
            if product.ingredient_ids:
                product.standard_price = sum(product.ingredient_ids.mapped("cost"))
            else:
                product.standard_price = product.stored_standard_price

    @api.multi
    def _inverse_standard_price(self):
        for product in self:
            product.stored_standard_price = product.standard_price

    @api.multi
    def production_wizard(self):
        self.ensure_one()
        return self.env["recipe.produce"]._create_from_product(self)