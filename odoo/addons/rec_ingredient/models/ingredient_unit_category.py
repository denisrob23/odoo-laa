# Copyright 2019 Denis
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, fields, models, _


class IngredientUnitCategory(models.Model):

    _name = 'ingredient.unit.category'
    _description = 'Ingredient Unit Category'

    name = fields.Char(required=True)
