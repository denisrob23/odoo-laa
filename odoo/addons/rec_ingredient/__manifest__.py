# Copyright 2019 Denis
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'Rec Ingredient',
    'description': """
        add ingrediants to product""",
    'version': '13.0.1.0.0',
    'license': 'AGPL-3',
    'author': 'Denis',
    'depends': [
        'product',
    ],
    'data': [
        'security/ingredient_unit_category.xml',
        'security/ingredient_unit.xml',
        'security/product_ingredient.xml',
        'views/menu.xml',
        'views/ingredient_unit_category.xml',
        'views/ingredient_unit.xml',
        'views/product_ingredient.xml',
        'views/product_template.xml',
        'wizards/recipe_produce.xml',
        'data/ingredient_unit_category.xml',
        'data/ingredient_unit.xml',
        'report/recipe_report.xml',
    ],
}
