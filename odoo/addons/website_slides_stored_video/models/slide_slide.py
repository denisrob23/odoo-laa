# Copyright 2019 Kerberross
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, fields, models, _


class SlideSlide(models.Model):

    _inherit = 'slide.slide'

    def _find_document_data_from_url(self, url):
        res = super()._find_document_data_from_url(url)
        if not res[0]:
            return ("data", url)
        return res

    def _parse_data_document(self, document_id, only_preview_fields):
        return {"values": {"document_id": "1"}}

    @api.depends('document_id', 'slide_type', 'mime_type', 'url')
    def _compute_embed_code(self):
        res = super()._compute_embed_code()
        for record in self:
            if record.slide_type == 'video' and record.document_id == "1":  # flemme
                record.embed_code = '<iframe src="%s" allowFullScreen="true" frameborder="0"></iframe>' % (record.url)
        return res
