# Copyright 2019 Kerberross
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'Website Slides Stored Video',
    'description': """
        Allow to have a stored video in courses""",
    'version': '13.0.1.0.0',
    'license': 'AGPL-3',
    'author': 'Kerberross',
    'depends': [
        'website_slides',
    ],
    'data': [
        'views/slide_slide.xml',
    ],
    'demo': [
    ],
}
