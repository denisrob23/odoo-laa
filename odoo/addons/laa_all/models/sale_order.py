from odoo import _, api, models


class SaleOrder(models.Model):
    _inherit = "sale.order"

    @api.model
    def create_order_from_pos(self, order_data, action):
        res = super().create_order_from_pos(order_data, action)
        if action == "delivered":
            so = self.env["sale.order"].browse(res["sale_order_id"])
            so._create_invoices()
            so.mapped('invoice_ids').action_post()
        return res
